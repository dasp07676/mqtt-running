const { json } = require("express");
const express = require("express");
const moment = require("moment");
const app = express();
const port = 8000;

// Connecting with redis
const redis = require("redis");
const redis_port = 6379;
const redisClient = redis.createClient(redis_port);

app.use(express.json());

app.patch("/update/:id", (req, res) => {
    const id = req.params.id;
    const status_now = req.body.Status;

    redisClient.get(id, (error, data) => {
        if (error) {
            res.status(400).send(error);
        }
        if (data) {
            const allData = JSON.parse(data);
            // console.log(allData.status);
            const threshold = moment().subtract(1, "hour");
            const last = moment(allData.Date);

            const obj = {
                "id": id,
                "Timestamp": allData.Timestamp,
                "Topic": allData.Topic,
                "Status": status_now,
                "Power": allData.Power,
                "Battery": allData.Battery,
                "Tamper": allData.Tamper,
                "Date": allData.Date
            }

            redisClient.set(id, JSON.stringify(obj));
            res.send({ ...obj, "isOnline" : last.isAfter(threshold),
                "Difference" : moment.duration(moment().subtract(last)).humanize()
            });
            console.log(obj);

        } else {
            res.status(400).send({
                "Message": "Data not found!"
            });
        }
    })
});

// Get Redis data by id only
app.get("/get/:id", (req, res) => {
    const id = String(req.params.id);
    redisClient.get(id, (error, data) => {
        if (error) {
            res.status(400).send(error);
        }
        if (data) {
            const allData = JSON.parse(data);
            const threshold = moment().subtract(1, "hour");
            const last = moment(allData.Date);

            res.status(200).send({
                ...allData, "isOnline": last.isAfter(threshold),
                "Difference": moment.duration(moment().subtract(last)).humanize()
            });

            console.log({
                ...allData, "isOnline": last.isAfter(threshold),
                "Difference": moment.duration(moment().subtract(last)).humanize()
            })

        } else {
            res.status(400).send({
                "Message": "Data not found!"
            });
        }
    })
})

// Get Redis all data
app.get("/get", (req, res) => {
    redisClient.keys("*", async (err, keys) => {
        let res_data = [];

        // console.log('keys::', keys)
        if (err) {
            res.status(400).send(err);
        }

        for (var i = 0, len = keys.length; i < len; i++) {
            redisClient.get(keys[i], (error, data) => {
                if (error) {
                    res.status(400).send(error);
                }
                const allData = JSON.parse(data);
                const obj = {
                    "id": allData._id,
                    "Timestamp": allData.Timestamp,
                    "Topic": allData.Topic,
                    "Status": allData.Status,
                    "Power": allData.Power,
                    "Battery": allData.Battery,
                    "Tamper": allData.Tamper,
                    "Date": allData.Date
                }
                
                const threshold = moment().subtract(1, "hour");
                const last = moment(obj.Date);
                res_data.push({ ...obj, isOnline: last.isAfter(threshold), difference: moment.duration(moment().subtract(last)).humanize() });
            })
        }
        setTimeout(() => {
            console.log('inside setTimeout::')
            console.log(res_data)
            res.send(res_data);
        }, 1000)
    })
})

app.listen(port, () => {
    console.log("Server running at ", port);
})