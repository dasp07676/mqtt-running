const { json } = require("express");
const express = require("express");
const app = express();
// const port = 7000;
var mqtt = require("mqtt");

// To check a mosquitto broker
// mosquitto_sub -p 1883 -h "lora_server.distronix.in" -t "#"

// For connecting Redis
const redis = require("redis");
// const redis_port = 6379;
const redisClient = redis.createClient({
    // host: "redis-server",
    port: 6379
});

// Broker url
const url = "lora_server.distronix.in";

const brokerURL = "mqtt://" + url;
const options = {
    // username: "distronix",
    // password: "distronix",
    keepalive: 1,
    port: 1883,
    clean: false,
    reconnectPeriod: 500,
    clientId: "mqttjs_" + Math.random().toString(16).substr(2, 8)
};
// Connecting with broker
const client = mqtt.connect(brokerURL, options)

// Checking if subscriber is listening or not
client.on("connect", (err) => {
    if (!err) {
        console.log("No error!!!");
    }
    console.log("Is subscriber ready to recieve? " + client.connected);
})

// Subscribing to a particular topic.We can also do "#" for all topics...
client.subscribe("#", function (err) {
    if (err) {
        console.log("Error while getting the data..." + JSON.stringify(err));
    }
})

// If subscriber is online, subscriber will get some messages as response...
client.on("message", function (topic, message) {
    const jsondata = JSON.parse(message);
    // console.log(jsondata.node);

    // If there is no "node" child in the json format we need to check that and also console that for better understanding
    if (jsondata.node == undefined) {
        console.log(jsondata);
    }
    // Creating a json format for storing the values in redis cache and also in mongodb
    else {
        const timestamp = jsondata.node.timestamp;
        // console.log(jsondata.node);
        const getnodedata = (id) => {
            const item = jsondata.node.data.find(i => i["s_id"] === id);
            if(typeof item === 'undefined') return "NA";
            else return item.val;
        }
        const new_node = {
            "_id": jsondata.node.id,
            // "Sensor_id": jsondata.node.id,
            "Timestamp": jsondata.node.timestamp,
            "Topic": topic,
            "Power": getnodedata("23000A01"),
            // jsondata.node.data.find(i => i["s_id"] === "23000A01").val,
            "Network": getnodedata("23000C01"),
            // jsondata.node.data.find(i => i["s_id"] === "23000C01").val
            "Battery": getnodedata("23000B01"),
            // jsondata.node.data.find(i => i["s_id"] === "23000B01").val
            "Tamper": getnodedata("23000901"),
            // jsondata.node.data.find(i => i["s_id"] === "23000901").val
            "Date" : new Date()
        };
        const new_node_for_history = {
            "Timestamp": jsondata.node.timestamp,
            "Topic": topic,
            "Power": getnodedata("23000A01"),
            // jsondata.node.data.find(i => i["s_id"] === "23000A01").val,
            "Network": getnodedata("23000C01"),
            // jsondata.node.data.find(i => i["s_id"] === "23000C01").val,
            "Battery": getnodedata("23000B01"),
            "Tamper": getnodedata("23000901"),
            "Date" : new Date()
        };

        const id = new_node["_id"];
        // console.log(id);

        redisClient.get(id, (err, data) => {
            let history = [];
            let Status = "Default";
            if(data) {
                const oldnode = JSON.parse(data);
                history = oldnode.history;
                Status = oldnode.Status;
            }
            const val = JSON.stringify({...new_node, Status, history: [ {...new_node_for_history, Status} ,...history]})
            redisClient.set(id, val);
            console.log(JSON.parse(val));
        })

        console.log("---------------------------------------------------");
        // console.table(jsondata.node);    
        // console.log(new_node);
        console.log("Data Inserted");
        // console.log({ jsondata, topic })
        console.log("---------------------------------------------------");
    }
});